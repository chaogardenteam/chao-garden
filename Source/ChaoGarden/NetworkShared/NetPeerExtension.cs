﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.NetworkShared
{
    public static class NetPeerExtension
    {
        public static NetOutgoingMessage CreateMessage(this NetServer server, ServerMessageType messageType)
        {
            NetOutgoingMessage result = server.CreateMessage();
            result.Write((Byte)messageType);
            return result;
        }

        public static NetOutgoingMessage CreateMessage(this NetServer server, ServerErrorType errorType)
        {
            NetOutgoingMessage result = server.CreateMessage();
            result.Write((Byte)ServerMessageType.Error);
            result.Write((Byte)errorType);
            return result;
        }

        public static NetOutgoingMessage CreateMessage(this NetServer server, ServerErrorType errorType, String errorDescription)
        {
            NetOutgoingMessage result = CreateMessage(server, errorType);
            result.Write(errorDescription);
            return result;
        }

        public static NetOutgoingMessage CreateMessage(this NetClient client, ClientMessageType messageType)
        {
            NetOutgoingMessage result = client.CreateMessage();
            result.Write((Byte)messageType);
            return result;
        }
    }
}
