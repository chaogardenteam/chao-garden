﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden.NetworkShared
{
    public class ServerReturnedErrorException : Exception
    {
        public ServerReturnedErrorException(String message)
            : base(message)
        {
        }
    }
}
