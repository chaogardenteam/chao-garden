﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace ChaoGarden.NetworkShared
{
    public enum ClientMessageType
    {
        Error,
        Login,
        Logout
    }

    public enum ServerMessageType
    {
        Error,
        WorldState,
        LoginSuccess
    }

    public enum ClientErrorType
    {
        Unknown
    }

    public enum ServerErrorType
    {
        Unknown,
        AuthenticationError
    }

    public delegate NetOutgoingMessage ServerMessageProcessing(NetServer server, NetIncomingMessage message);
    public delegate void ClientMessageProcessor(NetIncomingMessage message);
}
