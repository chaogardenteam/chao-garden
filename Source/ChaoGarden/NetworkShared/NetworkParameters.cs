﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden.NetworkShared
{
    public static class NetworkParameters
    {
        public const NetDeliveryMethod DefaultDeliveryMethod = NetDeliveryMethod.ReliableOrdered;
        public const String ApplicationName = "game";
        public const Int32 DefaultServerPort = 14242;
        public static readonly TimeSpan DefaultServerWorkStep = TimeSpan.FromSeconds(1.0 / 60.0);
    }
}
