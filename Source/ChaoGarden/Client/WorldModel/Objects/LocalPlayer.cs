﻿using System;
using Microsoft.Xna.Framework;
using ChaoGarden.GraphicsModel;

namespace ChaoGarden.WorldModel
{
    public class LocalPlayer : Player
    {
        public LocalPlayer (Game game, Scene scene, Camera camera, Level level)
            : base(game, scene, camera, level)
        {
        }
    }
}

