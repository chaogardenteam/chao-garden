﻿using System;
using Microsoft.Xna.Framework;
using ChaoGarden.GraphicsModel;

namespace ChaoGarden.WorldModel
{
    public class Player : Entity
    {
        private Level _level;
        private Node _node;
        private Camera _camera;

        public Player (Game game, Scene scene, Camera camera, Level level)
            : base(game, scene)
        {
            _camera = camera;
            _level = level;
            _node = new Node(@"Models/Sonic/Sonic", game.Content);
            scene.AddNode(_node);

            //_node.Position = new Vector3(10, 10, 70);
            //_camera.Position = new Vector3(30, 30, 100);
            _node.StartAnimation("walk");
        }

        public override void Update(GameTime time)
        {
            _node.Update(time);
        }
    }
}

