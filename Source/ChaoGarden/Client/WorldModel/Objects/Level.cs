﻿using System;
using Microsoft.Xna.Framework;
using ChaoGarden.GraphicsModel;

namespace ChaoGarden.WorldModel
{
    public class Level : Entity
    {
        string _levelName;
        private LevelNode _node;

        public Level (Game game, Scene scene, string levelName) : base(game, scene)
        {
            _levelName = levelName;
            _node = new LevelNode(String.Format(@"Models/{0}/{0}", _levelName), game.Content);
            //scene.AddNode(_node);
        }
    }
}

