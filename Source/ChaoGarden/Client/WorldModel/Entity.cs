﻿using System;
using Microsoft.Xna.Framework;
using ChaoGarden.GraphicsModel;

namespace ChaoGarden.WorldModel
{
    public class Entity
    {
        protected Guid _guid;
        protected Game _game;
        protected Scene _scene;

        public Guid Guid
        {
            get
            {
                return _guid;
            }
        }

        public Entity (Game game, Scene scene)
        {
            _guid = Guid.NewGuid();
            _game = game;
            _scene = scene;
        }

        public virtual void Update(GameTime time) { }
    }
}

