﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ChaoGarden.GraphicsModel;

namespace ChaoGarden.WorldModel
{
    /* TODO: Переименовать одноимённый класс в пакете ChaoServerWorldModel
     * во что-нибудь ещё во избежание путаницы */
    public class World
    {
        private Dictionary<Guid, Entity> _objects = new Dictionary<Guid, Entity>();
        private Game _game;
        private Camera _camera;
        private Scene _scene;

        public World (Game game)
        {
            _game = game;
            _camera = new Camera(new Vector3(2, 2, 2), Vector3.Zero, Vector3.Up,
                MathHelper.PiOver2, game.GraphicsDevice.DisplayMode.AspectRatio, 1, 2000);
            _scene = new Scene(game, _camera);
            _game.Components.Add(_scene);

            Level level = new Level(game, _scene, "chaostgneut");
            _objects.Add(level.Guid, level);
            LocalPlayer player = new LocalPlayer(game, _scene, _camera, level);
            _objects.Add(player.Guid, player);
        }

        public void Update(GameTime time)
        {
            foreach (Entity entity in _objects.Values)
                entity.Update(time);
        }
    }
}

