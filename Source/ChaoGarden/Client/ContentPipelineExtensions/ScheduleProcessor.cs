using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline;

// TODO: replace these with the processor input and output types.

namespace ChaoGarden.ContentProcessingExtensions
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "ChaoGarden.ContentProcessingExtensions.ScheduleProcessor")]
    public class ScheduleProcessor : ContentProcessor<List<AnimationScheduleEntry>, AnimationSchedule>
    {
        public override AnimationSchedule Process(List<AnimationScheduleEntry> input, ContentProcessorContext context)
        {
            String error = null;
            Check(input, out error);
            if (error == null)
                return new AnimationSchedule(input);
            else
                throw new InvalidContentException(error);
        }

        private void Check(List<AnimationScheduleEntry> list, out String error)
        {
            error = null;
            List<String> errors = new List<String>();
            for (Int32 firstIndex = 0; firstIndex < list.Count; firstIndex++)
            {
                for (Int32 secondIndex = firstIndex + 1; secondIndex < list.Count; secondIndex++)
                {
                    AnimationScheduleEntry first = (list[firstIndex].Begin < list[secondIndex].Begin) ? list[firstIndex] : list[secondIndex];
                    AnimationScheduleEntry second = (list[firstIndex].Begin < list[secondIndex].Begin) ? list[secondIndex] : list[firstIndex];
                    if (second.Begin >= first.End)
                    {
                        //then things good =)
                    }
                    else
                        errors.Add(first.Name + " and " + second.Name);
                }
            }

            if (errors.Count > 0)
            {
                error = String.Format("These animations have intersections in time, please correct them: {0}.", 
                    String.Join(", ", errors));
            }
        }
    }
}