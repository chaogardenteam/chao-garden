﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline
{
    public class AnimationSchedule : IEnumerable<AnimationScheduleEntry>
    {
        private Dictionary<String, AnimationScheduleEntry> _schedule;

        public AnimationSchedule(IEnumerable<AnimationScheduleEntry> futureSchedule)
        {
            _schedule = new Dictionary<String, AnimationScheduleEntry>(futureSchedule.ToDictionary((entry) => entry.Name));
        }

        public Boolean HaveThisAnimation(String name)
        {
            return _schedule.ContainsKey(name);
        }

        public TimeSpan GetBeginTime(String name)
        {
            return _schedule[name].Begin;
        }

        public TimeSpan GetNextTime(String name, TimeSpan currentTime, TimeSpan elapsedTime, out Boolean animationRestarted)
        {
            animationRestarted = false;
            TimeSpan result = currentTime + elapsedTime;
            if (result >= _schedule[name].End)
            {
                animationRestarted = true;
            }
            while (result >= _schedule[name].End)
            {
                result -= _schedule[name].Length;
            }
            return result;
        }

        public IEnumerator<AnimationScheduleEntry> GetEnumerator()
        {
            return _schedule.Values.ToList().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _schedule.Values.ToArray().GetEnumerator();
        }
    }
}
