﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline
{
    public class AnimationScheduleEntry
    {
        private TimeSpan _begin;
        private TimeSpan _end;
        private String _name;

        public TimeSpan Begin
        {
            get
            {
                return _begin;
            }
        }

        public TimeSpan End
        {
            get
            {
                return _end;
            }
        }

        public TimeSpan Length
        {
            get
            {
                return _end - _begin;
            }
        }

        public String Name
        {
            get
            {
                return _name;
            }
        }

        public AnimationScheduleEntry(TimeSpan begin, TimeSpan end, String name)
        {
            _begin = begin;
            _end = end;
            _name = name;
        }
    }
}
