using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline;

// TODO: replace this with the type you want to write out.

namespace ChaoGarden.ContentProcessingExtensions
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class ScheduleWriter : ContentTypeWriter<AnimationSchedule>
    {
        protected override void Write(ContentWriter output, AnimationSchedule value)
        {
            output.Write(value.Count());
            foreach (AnimationScheduleEntry entry in value)
            {
                output.Write(entry.Begin.Ticks);
                output.Write(entry.End.Ticks);
                output.Write(entry.Name);
            }
        }

        public override String GetRuntimeReader(TargetPlatform targetPlatform)
        {
            // TODO: change this to the name of your ContentTypeReader
            // class which will be used to load this data.
            return "ChaoGarden.GraphicsModel.GraphicsMaterialsReaders.AnimationScheduleReader, GraphicsModel";
        }
    }
}
