using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using System.IO;
using ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline;

namespace ChaoGarden.ContentProcessingExtensions
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to import a file from disk into the specified type, TImport.
    /// 
    /// This should be part of a Content Pipeline Extension Library project.
    /// 
    /// TODO: change the ContentImporter attribute to specify the correct file
    /// extension, display name, and default processor for this importer.
    /// </summary>
    [ContentImporter(".txt", DisplayName = "Schedule Importer", DefaultProcessor = "ScheduleProcessor")]
    public class ScheduleImporter : ContentImporter<List<AnimationScheduleEntry>>
    {
        public override List<AnimationScheduleEntry> Import(String filename, ContentImporterContext context)
        {
            List<AnimationScheduleEntry> futureSchedule = new List<AnimationScheduleEntry>();
            using (StreamReader reader = new StreamReader(filename))
            {
                String currentAnimationTime;
                Int32 stringNumber = 0;
                while ((currentAnimationTime = reader.ReadLine()) != null)
                {
                    stringNumber++;
                    String errorMessage = String.Format("At string number {0}: ", stringNumber);
                    try
                    {
                        String[] entryPieces = currentAnimationTime.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        TimeSpan begin = ParseTime(entryPieces[0]);
                        TimeSpan end = ParseTime(entryPieces[1]);
                        String name = entryPieces[2];
                        if (begin >= end)
                            throw new InvalidContentException(errorMessage + "Mismatch begin and end time of animation");
                        futureSchedule.Add(new AnimationScheduleEntry(begin, end, name));
                    }
                    catch (InvalidContentException ex)
                    {
                        throw ex;
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        throw new InvalidContentException(errorMessage + "Not enough data: " + ex.Message);
                    }
                    catch (FormatException ex)
                    {
                        throw new InvalidContentException(errorMessage + "Parse error: " + ex.Message);
                    }
                }
            }
            return futureSchedule;
        }

        private TimeSpan ParseTime(String source)
        {
            Int32 delPos = source.IndexOf(':');
            Int32 minutes = Int32.Parse(source.Substring(0, delPos));
            return TimeSpan.FromMinutes(minutes) + TimeSpan.ParseExact(source.Substring(delPos+1), "ss\\:ff", null);
        }
    }
}
