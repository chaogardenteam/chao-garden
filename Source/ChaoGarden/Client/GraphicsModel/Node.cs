﻿using ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden.GraphicsModel
{
    public class Node
    {
        private Model _model = null;
        private AnimationPlayer _animationPlayer;
        private String _modelLocation;
        private ContentManager _manager;
        private Matrix _world = Matrix.Identity;
        private Vector3 _position;

        public Matrix World
        {
            get
            {
                return _world;
            }
            set
            {
                _world = value;
            }
        }

        public Boolean ModelIsLoaded
        {
            get
            {
                return _model != null;
            }
        }

        public Boolean HasAnimation
        {
            get
            {
                return _animationPlayer != null;
            }
        }

        public AnimationPlayer AnimationPlayer
        {
            get
            {
                return _animationPlayer;
            }
        }

        protected Boolean AnimationIsPlayed
        {
            get
            {
                if (_animationPlayer == null)
                    return false;
                return _animationPlayer.AnimationIsPlayedNow;
            }
        }

        public Vector3 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        public Node (String modelLocation, ContentManager manager)
        {
            _modelLocation = modelLocation;
            _manager = manager;
        }

        public SkinningData AnimationData
        {
            get
            {
                return _model.Tag as SkinningData;
            }
        }

        public void  LoadModel()
        {
            _model = _manager.Load<Model>(_modelLocation);
            if (_model.Tag is SkinningData)
            {
                _animationPlayer = new AnimationPlayer((SkinningData)_model.Tag);

                foreach (ModelMesh mesh in _model.Meshes)
                {
                    foreach (ModelMeshPart part in mesh.MeshParts)
                    {
                        CustomSkinnedEffect effect = new CustomSkinnedEffect(part.Effect.GraphicsDevice);
                        BasicEffect basicEffect = part.Effect as BasicEffect;
                        if (basicEffect != null)
                        {
                            effect.Texture = basicEffect.Texture;
                        }
                        part.Effect = effect;
                    }
                }
            }                
            foreach (ModelMesh mesh in _model.Meshes)
            {
                foreach (Effect e in mesh.Effects)
                {
                    if (e is IEffectLights)
                    {
                        IEffectLights lightsEffect = (IEffectLights)e;
                        lightsEffect.LightingEnabled = true; // Turn on the lighting subsystem.

                        lightsEffect.DirectionalLight0.DiffuseColor = new Vector3(1f, 0.2f, 0.2f); // a reddish light
                        lightsEffect.DirectionalLight0.Direction = new Vector3(0, 1, 0);  // coming along the x-axis
                        lightsEffect.DirectionalLight0.SpecularColor = new Vector3(0, 1, 0); // with green highlights

                        lightsEffect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f); // Add some overall ambient light.
                    }
                }
            }

        }

        public void Update(GameTime gameTime)
        {
            if (AnimationIsPlayed)
                _animationPlayer.Update(gameTime.ElapsedGameTime, true, _world);
        }

        public void StartAnimation(string animationName)
        {
            _animationPlayer.StartClip(animationName);
        }

        public void Draw(Matrix view, Matrix projection)
        {
            foreach (ModelMesh mesh in _model.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    IEffectMatrices positionEffect = effect as IEffectMatrices;
                    if (positionEffect != null)
                    {
                        positionEffect.World = _world * GetParentTransform(_model, mesh.ParentBone)
                            * Matrix.CreateTranslation(_position);
                        positionEffect.View = view;
                        positionEffect.Projection = projection;
                    }

                    CustomSkinnedEffect skinnedEffect = effect as CustomSkinnedEffect;
                    if (_animationPlayer != null && skinnedEffect != null)
                    {
                        skinnedEffect.SetBoneTransforms(AnimationPlayer.GetSkinTransforms());
                    }
                }
                mesh.Draw();
            }
        }

        //Надо бы это дело закэшировать
        private Matrix GetParentTransform(Model model, ModelBone bone)
        {
            ModelBone currentBone = bone;
            Matrix result = bone.Transform;
            while (currentBone != model.Root)
            {
                currentBone = currentBone.Parent;
                result = result * currentBone.Transform;
            }
            return result;
        }
    }
}
