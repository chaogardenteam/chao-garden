﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden.GraphicsModel
{
    public class Camera
    {
        private Vector3 _position;
        private Vector3 _target;
        private Vector3 _upDirection;

        private Single _angleOfView;
        private Single _aspectRatio;
        private Single _nearPlane;
        private Single _farPlane;

        public Matrix View
        {
            get
            {
                return Matrix.CreateLookAt(_position, _target, _upDirection);
            }
        }

        public Matrix Projection
        {
            get
            {
                return Matrix.CreatePerspectiveFieldOfView(_angleOfView, _aspectRatio, _nearPlane, _farPlane);
            }
        }

        public Vector3 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        public Camera(Vector3 position, Vector3 target, Vector3 up, 
            Single angleOfView, Single aspectRatio, Single nearPlane, Single farPlane)
        {
            _position = position;
            _target = target;
            _upDirection = up;
            _angleOfView = angleOfView;
            _aspectRatio = aspectRatio;
            _nearPlane = nearPlane;
            _farPlane = farPlane;
        }

        public void Draw(Node modelUser)
        {
            if (modelUser.ModelIsLoaded)
                modelUser.Draw(View, Projection);
            else
                throw new InvalidOperationException("Need to load model first");
        }

        public void MoveCamera(Vector3 moveDirection)
        {
            _position += moveDirection;
        }

        public void MoveTarget(Vector3 moveDirection)
        {
            _target += moveDirection;
        }
    }
}
