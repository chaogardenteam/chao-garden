﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using ChaoGarden.ContentProcessingExtensions.TypesForContentPipeline;

namespace ChaoGarden.GraphicsModel.GraphicsMaterialsReaders
{
    public class AnimationScheduleReader : ContentTypeReader<AnimationSchedule>
    {
        protected override AnimationSchedule Read(ContentReader input, AnimationSchedule existingInstance)
        {
            Int32 entriesCount = input.ReadInt32();
            List<AnimationScheduleEntry> futureSchedule = new List<AnimationScheduleEntry>(entriesCount);
            for (Int32 i = 0; i < entriesCount; i++)
            {
                TimeSpan begin = TimeSpan.FromTicks(input.ReadInt64());
                TimeSpan end = TimeSpan.FromTicks(input.ReadInt64());
                String name = input.ReadString();
                futureSchedule.Add(new AnimationScheduleEntry(begin, end, name));
            }
            return new AnimationSchedule(futureSchedule);
        }
    }
}
