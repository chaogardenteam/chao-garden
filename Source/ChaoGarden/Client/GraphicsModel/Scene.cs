﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden.GraphicsModel
{
    public class Scene : DrawableGameComponent
    {
        private List<Node> _nodes = new List<Node>();
        private Camera _camera;

        public Camera Camera
        {
            get
            {
                return _camera;
            }
        }

        public Scene(Game game, Camera camera)
            : base(game)
        {
            _camera = camera;
        }
        public Node this [int index]
        {
            get
            {
                if (_nodes.Count < index)
                    return _nodes[index];
                else
                    return null;
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (Node node in _nodes)
                node.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Node node in _nodes)
                _camera.Draw(node);
            base.Update(gameTime);
        }

        public void AddNode(Node node)
        {
            _nodes.Add(node);
            if (!node.ModelIsLoaded)
                node.LoadModel();
        }

        public void RemoveNode(Node node)
        {
            _nodes.Remove(node);
        }
    }
}
