﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using ChaoGarden.GraphicsModel;
using ChaoGarden.NetworkClient;
using ChaoGarden.NetworkShared;
using ChaoGarden.InitLogic;
#endregion

namespace ChaoGarden
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameGateway : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private ErrorViewComponent _errorView;
        private Director _director;
        private ChaonetDispatcher _dispatcher;

        public GameGateway()
            : base()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _director = new Director(this);

            Dictionary<ServerMessageType, ClientMessageProcessor> outerProcessors = new Dictionary<ServerMessageType, ClientMessageProcessor>()
            {
                {ServerMessageType.WorldState, _director.GetWorldStateFromNetwork }
            };
            ChaonetEntryPoint entryPoint = NetworkInit.InitFrom("network.json");
            _dispatcher = new ChaonetDispatcher(outerProcessors, entryPoint);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _errorView = new ErrorViewComponent(this, Content.Load<SpriteFont>(@"Fonts\ErrorFont"), new Vector2(0, 0));
            Components.Add(_errorView);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _dispatcher.Dispose();
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            try
            {
                _dispatcher.ProcessIncome();
            }
            catch (ServerReturnedErrorException ex)
            {
                _errorView.State.SetFatalError(ex.Message);
            }
            //if (!_errorView.State.ErrorIsPresent)
            //_director.UpdateScene();
            _director.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            base.Draw(gameTime);
        }

        private void FatalErrorRoutine(object sender, EventArgs e)
        {
            //_director.TurnOff();
            // TODO: Выдавать какое-нибудь окно об ошибке
        }
    }
}
