﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden
{
    public class ErrorViewComponent : DrawableGameComponent
    {
        private ErrorState _errorState;
        private Vector2 _position;
        private SpriteFont _spriteFont;

        public ErrorState State
        {
            get
            {
                return _errorState;
            }
        }

        public ErrorViewComponent(Game game, SpriteFont spriteFont, Vector2 position) 
            : base(game)
        {
            _spriteFont = spriteFont;
            _position = position;
            _errorState = new ErrorState();
        }

        public override void Draw(GameTime gameTime)
        {
            if (_errorState.ErrorIsPresent)
            {
                SpriteBatch spriteBatch = new SpriteBatch(Game.GraphicsDevice);
                spriteBatch.Begin();
                spriteBatch.DrawString(_spriteFont, _errorState.ErrorMessage, _position, 
                    _errorState.NeedToRestart ? Color.Red : Color.Yellow);
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }

    }
}
