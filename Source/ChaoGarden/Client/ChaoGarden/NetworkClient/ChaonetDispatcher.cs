﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using System.Net;
using ChaoGarden.NetworkShared;
using System.Diagnostics;

namespace ChaoGarden.NetworkClient
{
    public class ChaonetDispatcher : IDisposable
    {
        private NetClient _client;
        private ChaonetEntryPoint _entryPoint;
        private Dictionary<ServerMessageType, ClientMessageProcessor> _messageProcessors;

        private NetClient Client
        {
            get
            {
                if (_client != null)
                    return _client;
                else
                    throw new ObjectDisposedException("NetClient");
            }
            set
            {
                _client = value;
            }
        }

        public ChaonetDispatcher(Dictionary<ServerMessageType, ClientMessageProcessor> outerProcessors, ChaonetEntryPoint entryPoint)
        {
            _entryPoint = entryPoint;
            _messageProcessors = new Dictionary<ServerMessageType,ClientMessageProcessor>();
            GetInnerProcessors().Concat(outerProcessors).ToList().ForEach((item) => _messageProcessors.Add(item.Key, item.Value));

            NetPeerConfiguration config = new NetPeerConfiguration(NetworkParameters.ApplicationName);
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.AcceptIncomingConnections = true;
            
            Client = new NetClient(config);
            Client.Start();

            Connect(entryPoint);
        }

        private void Connect(ChaonetEntryPoint entryPoint)
        {
            Client.Connect(entryPoint.ServerAddress, entryPoint.ServerPort);
        }

        private void TryToAuthenticate(ChaonetEntryPoint entryPoint)
        {
            NetOutgoingMessage authenticate = Client.CreateMessage(ClientMessageType.Login);
            authenticate.Write(entryPoint.Username);
            authenticate.Write(entryPoint.Password);

            Client.SendMessage(authenticate, NetworkParameters.DefaultDeliveryMethod);
        }

        public void ProcessIncome()
        {
            NetIncomingMessage message;
            while ((message = Client.ReadMessage()) != null)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        ServerMessageType messageType = (ServerMessageType)message.ReadByte();
                        ClientMessageProcessor messageProcessor = GetMessageProcessor(messageType);
                        messageProcessor(message);
                    break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)message.ReadByte();
                        String description = message.ReadString();
                        if (status == NetConnectionStatus.Connected)
                            TryToAuthenticate(_entryPoint);
                        Trace.WriteLine(message.MessageType + " " + status + " " + description);
                    break;
                }
            }
        }

        private ClientMessageProcessor GetMessageProcessor(ServerMessageType messageType)
        {
            return _messageProcessors[messageType];
        }

        private Dictionary<ServerMessageType, ClientMessageProcessor> GetInnerProcessors()
        {
            return new Dictionary<ServerMessageType, ClientMessageProcessor>
            {
                {ServerMessageType.Error, ErrorProcess},
                {ServerMessageType.LoginSuccess, SuccessLoginProcess }
            };
        }

        #region inner message processors
        private void ErrorProcess(NetIncomingMessage message)
        {
            ServerErrorType errorType = (ServerErrorType)message.ReadByte();
            String errorDescription = message.ReadString();
            throw new ServerReturnedErrorException(errorType.ToString() + ": " + errorDescription);
        }

        private void SuccessLoginProcess(NetIncomingMessage message)
        {
        }
        #endregion

        public void Dispose()
        {
            NetOutgoingMessage logOutMessage = Client.CreateMessage(ClientMessageType.Logout);
            logOutMessage.Write(_entryPoint.Username);
            Client.SendMessage(logOutMessage, NetDeliveryMethod.ReliableOrdered);
            Client.Disconnect("");
            DisposeClient();
        }

        private void DisposeClient()
        {
            Client = null;
        }
    }
}
