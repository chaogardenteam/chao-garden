﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.NetworkClient
{
    [JsonObject(MemberSerialization.Fields)]
    public class ChaonetEntryPoint
    {
        private String _serverAddress;
        private String _username;
        private String _password;
        private Int32 _serverPort;

        public String ServerAddress
        {
            get
            {
                return _serverAddress;
            }
        }

        public Int32 ServerPort
        {
            get
            {
                return _serverPort;
            }
        }

        public String Username
        {
            get
            {
                return _username;
            }
        }

        public String Password
        {
            get
            {
                return _password;
            }
        }

        public ChaonetEntryPoint(String serverAddress, Int32 serverPort, String username, String password)
        {
            IPAddress forAttemptToParse;

            _serverAddress = serverAddress;
            _serverPort = serverPort;
            _username = username;
            _password = password;
        }
    }
}
