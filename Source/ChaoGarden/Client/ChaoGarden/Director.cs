﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChaoGarden.GraphicsModel;
using ChaoGarden.WorldModel;

namespace ChaoGarden
{
    public class Director
    {
        private Game _game;
        private World _world;

        public void GetWorldStateFromNetwork(NetIncomingMessage message)
        {
        }

        public Director(Game game)
        {
            _game = game;
            _world = new World(game);
        }

        public void Update(GameTime gameTime)
        {
            _world.Update(gameTime);
        }
    }
}
