﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaoGarden
{
    public class ErrorState
    {
        private String _errorMessage = null;
        private Boolean _needToRestart = false;

        public String ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        public Boolean ErrorIsPresent
        {
            get
            {
                return _errorMessage != null;
            }
        }

        public Boolean NeedToRestart
        {
            get
            {
                return _needToRestart;
            }
        }

        public event EventHandler FatalErrorHappen;
        public event EventHandler ErrorHappen;

        public void SetError(String errorMessage)
        {
            _errorMessage = errorMessage;
            if (ErrorHappen != null)
                ErrorHappen(this, EventArgs.Empty);
        }

        public void SetFatalError(String errorMessage)
        {
            _errorMessage = errorMessage;
            _needToRestart = true;
            if (FatalErrorHappen != null)
                FatalErrorHappen(this, EventArgs.Empty);
        }
    }
}
