﻿using ChaoGarden.NetworkClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace ChaoGarden.InitLogic
{
    public static class NetworkInit
    {
        public static ChaonetEntryPoint InitFrom(String path)
        {
            String jsonEntryPoint;
            using (StreamReader reader = new StreamReader(path))
            {
                jsonEntryPoint = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<ChaonetEntryPoint>(jsonEntryPoint);
        }
    }
}
