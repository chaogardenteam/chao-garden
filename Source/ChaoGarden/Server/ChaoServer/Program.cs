﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChaoGarden.ServerWorldModel;
using ChaoGarden.NetworkShared;

namespace ChaoGarden.ChaoServer
{
    class Program
    {
        private const Int32 DefaultPort = 54100;
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new TextWriterTraceListener("Log.txt"));
            Trace.Listeners.Add(new ConsoleTraceListener());
            try
            {
                WorldRuler ruler = new WorldRuler(new World(), NetworkParameters.DefaultServerWorkStep);
                ruler.Start();
                Trace.WriteLine("Listening on :" + NetworkParameters.DefaultServerPort);

                String command = "";
                while (command != "exit")
                    command = Console.ReadLine();
                ruler.Shutdown();
                ruler.ServerShutdownOccured.WaitOne();
            }
            finally
            {
                Trace.Flush();
            }
        }
    }
}
