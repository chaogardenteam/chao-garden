﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChaoGarden.ServerWorldModel;
using System.Threading;

namespace ChaoGarden.ChaoServer
{
    class WorldRuler
    {
        private World _world;
        private NetworkDispatcher _network;
        private TimeSpan _minStep;
        private TimeSpan _lastStep = TimeSpan.Zero;
        private Stopwatch _stepMeasurer = new Stopwatch();


        private Thread _workThread;
        private ManualResetEvent _serverShutdownOccured = new ManualResetEvent(false);
        private Boolean _shutdown = false;

        public Boolean IsWork
        {
            get
            {
                return _workThread.IsAlive;
            }
        }

        public ManualResetEvent ServerShutdownOccured
        {
            get
            {
                return _serverShutdownOccured;
            }
        }

        public WorldRuler(World world, TimeSpan minStep)
        {
            _world = world;
            _minStep = minStep;
            _workThread = new Thread(Work);
            _network = new NetworkDispatcher();
        }

        public void Start()
        {
            if (!IsWork)
                _workThread.Start();
            else
                throw new InvalidOperationException("World ruler already started");
        }

        public void Shutdown()
        {
            _shutdown = true;
        }

        private void Work()
        {
            _network.Start();

            while (!_shutdown)
                Step();
            
            _serverShutdownOccured.Set();
        }

        private void Step()
        {
            _stepMeasurer.Restart();
            if (_lastStep != TimeSpan.Zero)
                WorkWithWorld(_lastStep);
            if (_stepMeasurer.Elapsed < _minStep)
            {
                Thread.Sleep(_minStep - _stepMeasurer.Elapsed);
            }
            _lastStep = _stepMeasurer.Elapsed;
        }

        private void WorkWithWorld(TimeSpan elapsedTime)
        {
            _network.ProcessIncome();
            _world.Update(elapsedTime);
            Dictionary<String, WorldState> worldStates = new Dictionary<String, WorldState>();
            List<String> users = _network.GetUsersOnline();
            foreach (String username in users)
            {
                worldStates.Add(username, _world.GetWorldState(username));
            }
            _network.SendToUsers(worldStates, NetworkShared.ServerMessageType.WorldState);
        }
    }
}
