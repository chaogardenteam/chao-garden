﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.ChaoServer
{
    class UsersRegistry
    {
        private Dictionary<String, NetConnection> _usersOnline = new Dictionary<String, NetConnection>();

        public NetConnection this[String username]
        {
            get
            {
                return _usersOnline[username];
            }
        }

        public Boolean AlreadyOnline(String username)
        {
            return _usersOnline.ContainsKey(username);
        }

        public Boolean Authenticate(String username, String password)
        {
            return true;
        }

        public void LogIn(String username, NetConnection connection)
        {
            _usersOnline.Add(username, connection);
        }

        public void LogOff(String username, NetConnection connection)
        {
            if (_usersOnline[username] != connection)
                throw new InvalidOperationException("Wrong name of user");
            _usersOnline.Remove(username);
            connection.Disconnect("LogOff");
        }

        public List<String>UsersOnline()
        {
            return _usersOnline.Keys.ToList();
        }
    }
}
