﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using System.Net;
using ChaoGarden.NetworkShared;
using System.Diagnostics;

namespace ChaoGarden.ChaoServer
{
    public class NetworkDispatcher
    {
        private Dictionary<ClientMessageType, ServerMessageProcessing> _messageProcessors;

        private NetServer _server;
        private UsersRegistry _usersOnline = new UsersRegistry();

        public NetworkDispatcher()
            : this(NetworkParameters.DefaultServerPort)
        {
        }
        public NetworkDispatcher(Int32 port)
        {
            InitializeMessageProcessors();
            NetPeerConfiguration config = new NetPeerConfiguration(NetworkParameters.ApplicationName);
            config.Port = port;

            _server = new NetServer(config);
        }

        public void ProcessIncome()
        {
            NetIncomingMessage message;
            while ((message = _server.ReadMessage()) != null)
            {
                Trace.Write("Message type: " + message.MessageType + ". ");
                switch(message.MessageType)
                {
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)message.ReadByte();
                        String description = message.ReadString();
                        Trace.Write(message.SenderConnection.RemoteEndPoint + ":" + status + " " + description);
                        break;
                    case NetIncomingMessageType.Data:
                        ClientMessageType messageType = (ClientMessageType)message.ReadByte();
                        ServerMessageProcessing currentProcessor = GetProcessor(messageType);
                        NetOutgoingMessage immediateResponse = currentProcessor(_server, message);
                        if (immediateResponse != null)
                            message.SenderConnection.SendMessage(immediateResponse, NetworkParameters.DefaultDeliveryMethod, 0);
                        break;
                }
                Trace.WriteLine("");
            }
        }

        public void SendToUsers<T>(Dictionary<String, T> envelopes, ServerMessageType messageType)
        {
            foreach (KeyValuePair<String, T> item in envelopes)
            {
                NetOutgoingMessage message = _server.CreateMessage(messageType);
                message.WriteAllFields(item.Value);
                NetConnection connection = _usersOnline[item.Key];
                connection.SendMessage(message, NetworkParameters.DefaultDeliveryMethod, 0);
            }
        }

        public List<String> GetUsersOnline()
        {
            return _usersOnline.UsersOnline();
        }

        #region Message processors
        private NetOutgoingMessage ProcessLogin(NetServer server, NetIncomingMessage loginMessage)
        {
            NetOutgoingMessage response = null;

            String username = loginMessage.ReadString();
            if (_usersOnline.AlreadyOnline (username))
            {
                NetConnection connection = _usersOnline [username];
                try
                {
                    _usersOnline.LogOff(username, connection);
                }
                catch (InvalidOperationException ex)
                {
                    response = server.CreateMessage(ServerErrorType.AuthenticationError,
                        "User already online. Got error when disconnecting: " + ex.Message);
                    return response;
                }
            }
            String password = loginMessage.ReadString();
            if (_usersOnline.Authenticate(username, password))
            {
                _usersOnline.LogIn(username, loginMessage.SenderConnection);
                response = server.CreateMessage(ServerMessageType.LoginSuccess);
            }
            else
            {
                response = server.CreateMessage(ServerErrorType.AuthenticationError, "Wrong username or password");
            }

            return response;
        }

        private NetOutgoingMessage ProcessLogOff(NetServer server, NetIncomingMessage logoffMessage)
        {
            NetOutgoingMessage response = null;

            String username = logoffMessage.ReadString();
            if (_usersOnline.AlreadyOnline(username))
            {
                try
                {
                    _usersOnline.LogOff(username, logoffMessage.SenderConnection);
                }
                catch (InvalidOperationException ex)
                {
                    response = server.CreateMessage(ServerErrorType.AuthenticationError, ex.Message);
                }
            }

            return response;
        }
        #endregion

        #region Service methods
        private void InitializeMessageProcessors()
        {
            _messageProcessors = new Dictionary<ClientMessageType, ServerMessageProcessing>()
            {
                { ClientMessageType.Login, ProcessLogin },
                { ClientMessageType.Logout, ProcessLogOff }
            };
        }

        private ServerMessageProcessing GetProcessor(ClientMessageType messageType)
        {
            return _messageProcessors[messageType];
        }
        #endregion

        internal void Start()
        {
            _server.Start();
        }
    }
}
