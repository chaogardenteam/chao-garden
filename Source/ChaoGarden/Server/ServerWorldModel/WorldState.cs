﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.ServerWorldModel
{
    [Serializable]
    public class WorldState
    {
        private Single _objectPosition;

        public Single ObjectPosition
        {
            get
            {
                return _objectPosition;
            }
        }

        internal WorldState(Single objectPosition)
        {
            _objectPosition = objectPosition;
        }

        public static WorldState GetEmpty()
        {
            return new WorldState(0);
        }
    }
}
