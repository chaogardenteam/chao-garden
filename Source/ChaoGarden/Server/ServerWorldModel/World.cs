﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden.ServerWorldModel
{
    public class World
    {
        private Single _objectPosition = 0;
        private TimeSpan _upTime = TimeSpan.Zero;

        public void Update(TimeSpan elapsedTime)
        {
            _upTime += elapsedTime;
            _objectPosition = 500 * (Single)Math.Sin(_upTime.TotalSeconds);
        }

        public WorldState GetWorldState(String username)
        {
            return new WorldState(_objectPosition);
        }
    }
}

